# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
# Copyright (c) 2019 David Hamner

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
import shutil
import sh
import time


try:
    from typing import List  # noqa: F401
except ImportError:
    pass

# Misc settings
debugging = True
dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = False
bring_front_click = True
cursor_warp = False
auto_fullscreen = False
focus_on_window_activation = "smart"
critical_debug = True
volume_app = "mate-volume-control"
# We're lying here. Nobody uses this string besides java UI toolkits
# LG3D happens to be on java's whitelist.
wmname = "LG3D"

# colors
bar_color = (47, 100, 70)
root_color = '#333333'
#                   graph     border
net_graph_color = ['EBBA18', '785521']

# var init
zoom = 0
zoom_time = 0
max_zoom = False
win_sizes = {}
last_menu_spot = [0,0]
last_window_position = (0, 0)
windows_moved = (0, 0)
min_win_size = 25
top_bar = 25
last_drag = None
window_with_mouse = None
hot_corner_triggered = None
moving_win = None
open_new_menu = True
found_menu = False
menu_window = ""
WIN = "mod4"
ALT = "mod1"
CTRL = "control"
snap_corners = []
snap_threshold = 25
snap_windows = []
menu_cmd = "xterm "
fullscreen_sizes = []
# load janit if installed
if shutil.which('janit') is not None:
    menu_cmd = menu_cmd + shutil.which('janit')


# Called after qtile starts managing a new client
@hook.subscribe.client_managed
def on_win_open(window):
    global open_new_menu
    global menu_window  # Janit menu

    if menu_window != "":
        # open new window over menu
        window.place(menu_window.x, menu_window.y, menu_window.width,
                     menu_window.height, 2, None)


@hook.subscribe.client_mouse_enter
def on_mouse_enter(window):
    global window_with_mouse
    window_with_mouse = window

#BUG Cannot hover and zoom
@hook.subscribe.client_focus
def focus_changed(window):
    global zoom
    global zoom_time
    global win_sizes
    global max_zoom
    global window_with_mouse
    
    window_with_mouse = window
    
    if zoom != 0:
        debug("RESET: 0")
        if time.time() - zoom_time > .3:
            debug(time.time())
            zoom = 0
            max_zoom = False
            update_zoom(window.qtile)
            needs_moved = [window.x, window.y - 25]
            for win in window.qtile.windows_map:
                win = window.qtile.windows_map.get(win)
                # blacklist the top Bar
                if win.group is None:
                    #win.focus()
                    continue
                
                win.place(win.x - needs_moved[0], win.y - needs_moved[1],
                    win.width, win.height, 2, None)
            win_sizes = {}
        else:
            debug("SLEEPING")

def debug(error, new_line='\n', level=1):
    global debugging
    global critical_debug
    crit_bug = level > 1
    crit_bug = crit_bug and critical_debug

    if debugging or crit_bug:
        debug_log = open('/tmp/Debug.txt', 'a+')
        debug_log.write(str(error) + new_line)
        debug_log.close()

def update_window_size(qtile):
    global win_sizes
    #TODO set focus to top bar
    debug("\n\n\nUpdate_SIZES")
    win_sizes = {}
    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # blacklist the top Bar
        if win.group is None:
            continue
        
        win_sizes[win] = [win.width, win.height, win.x, win.y]


def update_zoom(qtile):
    global zoom
    global win_sizes
    global max_zoom
    
    mouse_x, mouse_y = qtile.get_mouse_position()
    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # blacklist the top Bar
        if win.group is None:
            continue

        topleft_point = (win_sizes[win][2], win_sizes[win][3])
        lowright_point = (int(win_sizes[win][2] + win_sizes[win][0]), int(win_sizes[win][3] + win_sizes[win][1]/2))
        #lowright_point = (int(win.x + win.width), int(win.y + win.height/2))

        multiplier = 0.15 * zoom
        #debug(zoom)
        topleft_change_x = (mouse_x - topleft_point[0]) * multiplier
        topleft_change_y = (mouse_y - topleft_point[1]) * multiplier

        lowright_change_x = (mouse_x - lowright_point[0]) * multiplier
        lowright_change_y = (mouse_y - lowright_point[1]) * multiplier

        topleft_point = (int(topleft_point[0] - topleft_change_x),
                         int(topleft_point[1] - topleft_change_y))
        lowright_point = (int(lowright_point[0] - lowright_change_x),
                          int(lowright_point[1] - lowright_change_y))
        new_height = (lowright_point[1] - topleft_point[1]) * 2
        new_width = lowright_point[0] - topleft_point[0]
        if new_width < 15 or new_height < 15:
            max_zoom = True
        if not max_zoom:
            win.place(topleft_point[0], topleft_point[1],
                  new_width, new_height, 2, None)
        
# zoom all windows out
def handle_mousewheel_down(qtile, *args):
    global zoom
    global win_sizes
    global max_zoom
    global zoom_time
    zoom_time = time.time()
    #debug("SET clock")
    #debug(time.time())
    if zoom == 0 and win_sizes == {}:
        debug("zoom = 0 and win_sizes unkown")
        update_window_size(qtile)
    if not max_zoom:
        zoom = zoom - 1
        update_zoom(qtile)

#ctrl + alt + f
def window_fullscreen(qtile, *args):
    global display_size
    global fullscreen_sizes
    
    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        
        #only change the window in focus
        if not win.has_focus:
            continue
        
        # don't change top bar ever
        if win.group is None:
            continue
        
        mid_point = [win.x + win.width/2, win.y + win.height/2]
        left_offset = 0
        display_num = 0
        for display in fullscreen_sizes:
            if (mid_point[0] < display[0] + left_offset) and mid_point[0] > left_offset:
                break
            left_offset = left_offset + display[0]
            display_num = display_num + 1
        win.place(left_offset, 0, fullscreen_sizes[display_num][0],
                  fullscreen_sizes[display_num][1], 2, None)

# switch desktop left
def move_windows_left(qtile, *args):
    global display_size

    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # don't move top bar
        if win.group is None:
            continue

        topleft_point = (win.x + display_size[0], win.y)
        win.place(topleft_point[0], topleft_point[1],
                  win.width, win.height, 2, None)


# switch desktop right
def move_windows_right(qtile, *args):
    global display_size

    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # don't move top bar
        if win.group is None:
            continue

        topleft_point = (win.x - display_size[0], win.y)
        win.place(topleft_point[0], topleft_point[1],
                  win.width, win.height, 2, None)


# switch desktop up
def move_windows_up(qtile, *args):
    global max_display_height

    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # don't move top bar
        if win.group is None:
            continue

        topleft_point = (win.x, win.y + max_display_height)
        win.place(topleft_point[0], topleft_point[1],
                  win.width, win.height, 2, None)


# switch desktop down
def move_windows_down(qtile, *args):
    global max_display_height

    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # don't move top bar
        if win.group is None:
            continue

        topleft_point = (win.x, win.y - max_display_height)
        win.place(topleft_point[0], topleft_point[1],
                  win.width, win.height, 2, None)


def move_all_windows(qtile, *args):
    global windows_moved
    mouse_position = qtile.get_mouse_position()
    if mouse_position == windows_moved:
        return

    move = (mouse_position[0] - windows_moved[0],
            mouse_position[1] - windows_moved[1])
    # check if this is a new drag
    if abs(move[0]) > abs(args[0]):
        move = (args[0], move[1])
    if abs(move[1]) > abs(args[1]):
        move = (move[0], args[1])

    windows_moved = mouse_position

    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # don't move top bar
        if win.group is None:
            continue

        topleft_point = (win.x + move[0], win.y + move[1])
        win.place(topleft_point[0], topleft_point[1],
                  win.width, win.height, 2, None)
def snap_point(position):
    global snap_corners
    global snap_windows
    # move mouse to snap_corners if close
    for point in snap_corners + snap_windows:
        if abs(position[0] - point[0]) < snap_threshold:
            if abs(position[1] - point[1]) < snap_threshold:
                position = point
                break
    return position
                
#hot corner resize and moving windows
def move_window(qtile, *args):
    global last_window_position
    global window_with_mouse
    global hot_corner_triggered
    global last_drag
    global moving_win
    global display_size
    global snap_corners
    global snap_threshold
    global snap_windows
    global top_bar

    # reset drag window and hot coners if this is a different drag event
    if last_drag != qtile._drag:
        hot_corner_triggered = None
        moving_win = window_with_mouse
        moving_win.cmd_bring_to_front()
        update_window_coners(qtile)

    mouse_position = snap_point(qtile.get_mouse_position())

    move = (mouse_position[0] - last_window_position[0],
            mouse_position[1] - last_window_position[1])
    if abs(move[0]) > abs(args[0]):
        move = (args[0], move[1])
    if abs(move[1]) > abs(args[1]):
        move = (move[0], args[1])

    last_window_position = mouse_position
    last_drag = qtile._drag

    if moving_win != "":
        # check if we hit the top left corner
        if mouse_position[0] < 5 and mouse_position[1] < 5:
            hot_corner_triggered = (0, 0)
        # check if we hit the bottom right corner
        if mouse_position[0] + 5 >= display_size[0]:
            if mouse_position[1] + 5 >= display_size[1]:
                hot_corner_triggered = (display_size[0], display_size[1])

        new_win_size = [min_win_size, min_win_size]
        # top-left corner resize
        if hot_corner_triggered == (0, 0):
            # set new_win_size to new window size (But not smaller)
            if mouse_position[0] > new_win_size[0]:
                new_win_size[0] = mouse_position[0]
            if mouse_position[1] - top_bar > new_win_size[1]:
                new_win_size[1] = mouse_position[1] - top_bar
            moving_win.place(0, top_bar, new_win_size[0], new_win_size[1], 2, None)

        # low-right corner resize
        elif hot_corner_triggered == (display_size[0], display_size[1]):
            # set new_win_size to new window size (But not smaller)
            if mouse_position[0] < display_size[0] - new_win_size[0]:
                new_win_size[0] = display_size[0] - mouse_position[0]
            if mouse_position[1] < display_size[1] - new_win_size[1]:
                new_win_size[1] = display_size[1] - mouse_position[1]
            moving_win.place(mouse_position[0], mouse_position[1],
                             new_win_size[0], new_win_size[1], 2, None)

        # move window
        if hot_corner_triggered is None:
            new_position = (moving_win.x + move[0], moving_win.y + move[1])
            moving_win.place(new_position[0], new_position[1],
                             moving_win.width, moving_win.height, 2, None)


# alt + Button1 = open/move menu
def open_menu(qtile, *args):
    global open_new_menu
    global all_open_windows
    global found_menu
    global menu_window
    global snap_corners
    global snap_windows
    global snap_threshold
    global top_bar
    global last_menu_spot
    
    cursor_x_movement = args[0]
    cursor_y_movement = args[1]
    drag_atm_x = args[2]
    drag_atm_y = args[3]
    
    # move mouse to snap_corners if close
    #BUG: only works dragging down right
    top_left = snap_point([drag_atm_x - cursor_x_movement, drag_atm_y - cursor_y_movement])
    mouse_position = snap_point([drag_atm_x,drag_atm_y])
    #drag backwords
    if top_left[0] > mouse_position[0]:
        tmp = top_left[0]
        top_left[0] = mouse_position[0]
        mouse_position[0] = tmp
    if top_left[1] > mouse_position[1]:
        tmp = top_left[1]
        top_left[1] = mouse_position[1]
        mouse_position[1] = tmp   
    
    #don't cover top_bar
    if top_left[1] < top_bar:
        top_left[1] = top_bar
    
    if abs(top_left[0] - last_menu_spot[0]) > 25 or abs(top_left[1] - last_menu_spot[1]) > 25:
        update_window_coners(qtile)

    cursor_x_movement = mouse_position[0] - top_left[0]
    cursor_y_movement = mouse_position[1] - top_left[1]
    
    #cursor_movement will end up being the window size. 
    #Don't let that get too small
    if cursor_x_movement < min_win_size:
        cursor_x_movement = min_win_size
    if cursor_y_movement < min_win_size:
        cursor_y_movement = min_win_size
        
    if cursor_x_movement > min_win_size or cursor_y_movement > min_win_size:
        # old menu is no longer open
        if menu_window != "":
            if menu_window.info()['id'] not in list(qtile.windows_map):
                open_new_menu = True
                menu_window = ""

        # open new menu/terminal
        if open_new_menu:
            all_open_windows = list(qtile.windows_map)  # windows before
            # issuecomment-447843762
            # Thanks https://github.com/qtile/qtile/issues/1245
            qtile.cmd_spawn(menu_cmd)
            found_menu = False
            open_new_menu = False
        else:
            # resize opened menu
            # look for new menu window
            found_window = ""
            if not found_menu:
                for win in qtile.windows_map:
                    if win not in all_open_windows:
                        found_window = win
                        found_menu = True
                if not found_menu:
                    return

            if menu_window == "":
                menu_window = qtile.windows_map.get(found_window)
            
            win_size = mouse_position[0]
            # place menu
            last_menu_spot = [top_left[0],top_left[1]]
            menu_window.place(top_left[0],
                              top_left[1],
                              cursor_x_movement, cursor_y_movement, 2, None)
            menu_window.cmd_disable_maximize()
            menu_window.focus(warp=None)
            menu_window.cmd_bring_to_front()

        

def update_window_coners(qtile):
    global snap_windows
    snap_windows = []
    for win in qtile.windows_map:
        win = qtile.windows_map.get(win)
        # add the top bar
        if win.group is None:
            continue
        topleft_point  = [win.x,                  win.y]
        topright_point = [int(win.x + win.width), win.y]
        lowright_point = [int(win.x + win.width), int(win.y + win.height)]
        lowleft_point  = [win.x,                  int(win.y + win.height)]
        snap_windows.append(topleft_point)
        snap_windows.append(topright_point)
        snap_windows.append(lowright_point)
        snap_windows.append(lowleft_point)


# key bindings
keys = [
  Key([ALT, CTRL], "t",
      lazy.spawn("xterm")),                # open xterm
  Key([WIN],       "k",
      lazy.window.kill()),                 # kill window
  Key([CTRL, ALT], "r",
      lazy.restart()),                     # restart qtile
  Key([CTRL, ALT], "q",
      lazy.shutdown()),                    # quit qtile
  Key([CTRL, ALT], "f",
      lazy.function(window_fullscreen)),   # Fullscreen window
  Key([CTRL, ALT], "Left",
      lazy.function(move_windows_left)),   # move all windows left
  Key([CTRL, ALT], "Right",
      lazy.function(move_windows_right)),  # move all windows Right
  Key([CTRL, ALT], "Down",
      lazy.function(move_windows_down)),   # move all windows Down
  Key([CTRL, ALT], "Up",
      lazy.function(move_windows_up)),     # move all windows Up
  ]

# Drag layouts
mouse = [
  #Click([ALT], "Button4",                           # Zoom In (Not used)
        #lazy.function(handle_mousewheel_up)),
  Click([ALT], "Button5",                           # Zoom Out
        lazy.function(handle_mousewheel_down)),
  Drag([WIN],  "Button1",                           # Move single win
       lazy.function(move_window),      start=0),
  Drag([CTRL], "Button1",                           # Open/Move Menu
       lazy.function(open_menu),        start=0),
  Drag([ALT],  "Button1",                           # Move all win
       lazy.function(move_all_windows), start=0),
  Drag([WIN],  "Button3",                           # resize single win
       lazy.window.set_size_floating(), start=lazy.window.get_size()),
  ]

groups = [Group(i) for i in "asdfuiop"]

layouts = [
  layout.Floating(),
  layout.Stack(num_stacks=2)
  ]

widget_defaults = dict(
  font='sans',
  fontsize=12,
  padding=3,
  )
extension_defaults = widget_defaults.copy()
floating_layout = layout.Floating()

# Define battery widget settings
battery_name = ""
"""
import utile
battery_name = util.find_battery_name()
battery_settings   = dict(
  battery_name   = battery_name,
  low_percentage = 0.1,
  format         = "{percent:2.0%}",
)
battery_icon_settings = dict(
  battery_name = battery_name,
  theme_path   = os.path.expanduser("~/.config/qtile/icons/battery"),
)
"""


######################################################################
##########################TackList code###############################
######################################################################
#show windows on all screens
@property
def open_windows(self):
    windows = []
    for win in self.bar.qtile.windows_map:
        win = self.bar.qtile.windows_map.get(win)

        # blacklist the top Bar
        if win.group is None:
            continue
        windows.append(win)
    #TODO order windows
    return windows#self.bar.screen.group.windows
widget.TaskList.windows = open_windows

#set focus on clicked window
def TaskList_click(self, x, y, button):
    window = None
    current_win = self.bar.screen.group.current_window

    #if button == 1:
    window = self.get_clicked(x, y)
    if window and button == 1:
        needs_moved = [window.x, window.y - 25]
        for win in self.bar.qtile.windows_map:
            win = self.bar.qtile.windows_map.get(win)
            # blacklist the top Bar
            if win.group is None:
                continue
            
            win.place(win.x - needs_moved[0], win.y - needs_moved[1],
                win.width, win.height, 2, None)
        window.cmd_focus()
        window.cmd_bring_to_front()
    if window and button == 2:
        window.toggle_minimize()
    #right click to close
    if window and button == 3:
        window.kill()
    self.update()
widget.TaskList.button_press = TaskList_click


######################################################################
##########################Top bar stuff###############################
######################################################################
# Define bars on screens
def make_topbar(screen):
    global bar_color

    # All Screens
    widgets = [
      widget.TaskList(rounded=True, border='0000FF',
                      unfocused_border='215578'),
      widget.TextBox("Aralia 0.1.0", name="configName"),
      widget.Sep(),
      widget.TextBox("Volume: "),
      widget.Volume(fontsize=12, volume_app=volume_app),
      widget.Sep(),
      widget.Notify(),
      ]

    # Stuff to only show on first screen
    if screen == 0:
        if battery_name:
            widgets.extend([
              # Battery widget not tested
              # widget.BatteryIcon(**battery_icon_settings),
              # widget.Battery(**battery_settings),
              ])
        widgets.extend([
          widget.Systray(),
          widget.Sep(),
          widget.TextBox("CPU: "),
          widget.CPUGraph(),
          widget.TextBox("Net: "),
          widget.NetGraph(border_color=net_graph_color[1],
                          graph_color=net_graph_color[0]),
          widget.Clock(format='%a %d %b %Y %H:%M:%S',
                       font='xft:monospace', fontsize=12),
          widget.Sep(),
          widget.Spacer(length=8),
          ])
    return bar.Bar(widgets, 25, background=bar_color)


screens = [
  Screen(top=make_topbar(0)),
  ]


# Configure each screen found by qtile.
def screen_config(qtile):
    global display_size
    global max_display_height  # biggest screen height
    global snap_corners
    global fullscreen_sizes
    global top_bar

    # find screen_size
    display_size = [0, 0]
    max_display_height = 0
    num_screens = 0
    for screen in qtile.conn.pseudoscreens:
        num_screens = num_screens + 1
        fullscreen_sizes.append([screen.width, screen.height])
        # 1/2 screen snap
        mid_point = [int(display_size[0] + screen.width/2), screen.height]
        snap_corners.append(mid_point)
        snap_corners.append([mid_point[0], top_bar])
        # fullscreen snap
        fullscr_point = [int(display_size[0] + screen.width), screen.height]
        snap_corners.append(fullscr_point)
        snap_corners.append([fullscr_point[0], top_bar])
        # append all screen width
        display_size[0] = display_size[0] + screen.width
        # keep last screen height
        # We might have issues if the displays are different sizes
        display_size[1] = screen.height
        # find tallest screen
        if screen.height > max_display_height:
            max_display_height = screen.height
    return [Screen(top=make_topbar(i)) for i in range(num_screens)]


def main(qtile):
    global root_color
    # TODO, Remove 'sh' as dependency
    sh.nm_applet(_bg=True)
    sh.xsetroot(['-solid',root_color])
    qtile.config.screens = screen_config(qtile)
    qtile.cmd_info()
