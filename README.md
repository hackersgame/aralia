# Aralia

X11 desktop environment with a different way of doing things.
Qtile needs to be installed. This will replace your default config.

Status: development

Install: run ./setup.sh


TODO:
-----

Alt + F4 = close
Win + drag up should open menu
Shift + ctrl + alt + arrow = move other windows not in focus
Alt + mouse wheel needs to not lose precision
Corner snapping on window move
Check/Install Janit (Menu)
