#!/bin/bash
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#check for qtile
#https://stackoverflow.com/a/26759734/5282272
if ! [ -x "$(command -v qtile)" ]; then
    echo 'Error: qtile is not installed.' >&2
    echo -e 'http://docs.qtile.org/en/latest/manual/install/\n'
    echo "Install via PIP: pip3 install qtile"
    echo "Source install (dependencies):"
    echo "pip3 install xcffib"
    echo "pip3 install cairocffi"
    echo "Ubuntu: sudo apt-get install libpangocairo-1.0-0 python3-dbus" #TODO check this
    echo "openSuSE: zypper in libpango-1_0-0 python3-dbus-python"        #TODO check this
    echo -e "\nInstall Qtile:"
    echo "git clone git://github.com/qtile/qtile.git"
    echo "cd qtile"
    echo "pip3 install ."
    exit 1
fi

#TODO check verion qtile -verion

#check config file
CONFIG_FILE=~/.config/qtile/config.py
#check if we have a qtile config file
echo "Checking config"
if test -f "$CONFIG_FILE"
then
    #check if it is the same as ours
    if ! cmp $CONFIG_FILE ./config.py &> /dev/null
    then
        echo "Backing-up old qtile config file"
        mv $CONFIG_FILE $CONFIG_FILE.bk
        echo "Copying Aralia config"
        cp ./config.py $CONFIG_FILE
    fi
else
    #no file found.
    echo "Copying Aralia config"
    mkdir -p ~/.config/qtile &>/dev/null
    cp ./config.py $CONFIG_FILE
fi

echo "Config setup"

#check for deskop file
DESKTOP_FILE=/usr/share/xsessions/Aralia.desktop
echo "Checking login script"
if ! test -f "$DESKTOP_FILE"
then
    echo "Copying login script"
    sudo cp ./Aralia.desktop $DESKTOP_FILE
fi
echo "login script setup"
echo "Logout and select Aralia as the DE"
